const fs = require('fs');
const path = require("path");

const loginValidate = function(params){
    const loginData = JSON.parse(fs.readFileSync(path.join(__dirname + '/../database/login.json'), 'utf8'));
    for(let data of loginData){
        if(params.userId == data.mobile || params.userId == data.mail && params.password == data.password){
            return { success : true, data : {userId : params.userId }};
        }
    }
    return { success : false, err : {msg : "Wrong Username/Password" }};
}

module.exports = loginValidate;
