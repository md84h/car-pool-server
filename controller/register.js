const fs = require('fs');
const path = require("path");

const registerUser = function(params){
    return new Promise((resolve,reject)=>{
        try{
            const filePath = path.join(__dirname + '/../database/login.json');
            const loginData = JSON.parse(fs.readFileSync(filePath, 'utf8'));
            for(let data of loginData){
                if(params.mobile == data.mobile || params.mail == data.mail){
                    resolve({ success : false, err : {msg : "Already Registered with same email/mobile, kindly login using same." }});
                }
            }
            loginData.push({mail:params.mail,mobile:params.mobile,password:params.password});
            const buffer = new Buffer(JSON.stringify(loginData));
            fs.open(filePath, 'w', function(err, fd) {
                if (err) {
                    resolve({ success : false, err : {msg : "Something went wrong, Please try again." }});
                }
                fs.write(fd, buffer, 0, buffer.length, null, function(err) {
                    if (err){
                        resolve({ success : false, err : {msg : "Something went wrong, Please try again." }});
                    }
                    fs.close(fd, function() {
                        resolve({ success : true, data : {userId : params.mobile }});
                    });
                });
            });
        }catch(err){
            resolve(err);
        }
    })
}

module.exports = registerUser;
