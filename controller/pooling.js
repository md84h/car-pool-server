const fs = require('fs');
const path = require("path");

const carPolling = function(params){
    const carData = JSON.parse(fs.readFileSync(path.join(__dirname + '/../database/pooling.json'), 'utf8'));
    return { success : true, data : carData};
}

module.exports = carPolling;
