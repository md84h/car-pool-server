(function() {
    const express = require('express');
    const app = express();
    const compression = require('compression');
    const bodyParser = require('body-parser');
    const cors = require('cors');
    const http = require("http");
    const WebSocketServer = require('ws').Server;
    const loginValidate = require("./controller/login.js");
    const registerUser = require("./controller/register.js");
    const poolCar = require("./controller/pooling.js");

    app.use(cors());
    const server = http.createServer(app);
    app.use(bodyParser.json({ limit:'1mb' }));
    app.use(bodyParser.urlencoded({ limit:'1mb', extended:true }));
    app.post('/api/login', function(req,res,next){
         res.send(loginValidate(req.body));
    });
    app.post('/api/register', function(req,res,next){
         registerUser(req.body).then(response=>{
             res.send(response);
         });
    });
    app.get('/api/pool/:src/:dest', function(req,res,next){
         res.send(poolCar());
    });
    const wss = new WebSocketServer({server});
    wss.on('connection', function (ws) {
        ws.on('message', function (message) {
            ws.send(JSON.stringify(poolCar()));
        });
    });
    wss.on('close', function(){
        console.log('disconnected');
    });
    if(!module.parent) {
        server.listen(process.env.NODE_PORT || 8080, function() {
            let host = server.address().address;
            let port = server.address().port;
            console.log('server  is Listening ', host, port);
        });
    }
    module.exports = app;
})();
